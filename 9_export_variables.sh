#!/bin/bash

## Topic : Exporting Variables ##

echo $name
# Output :

name="Shivam Palaskar"
echo $name
# Output : Shivam Palaskar
echo

unset name

# To export a custom variable temporarily in environment variables
export name="Shivam Palaskar"

echo $name
# Output : Shivam Palaskar
echo

# To print all environment variables
env

# To print value of environment variable
printenv USER

# To check whether a variable is an environment variable or not
printenv name
# Output : value if it is a environment variable other wise blank

# To export a custom variable temporarily in environment variables
export name="Shivam Palaskar"

# To export a custom variable permanently in environment variables to a particular users session
# In `~/.bashrc` file (located in user's home directory) add export name="Shivam Palaskar"

# To export a custom variable permanently in environment variables to all users session
# In `/etc/profile` file add export name="Shivam Palaskar"

# export is used to set environment variable in operating system. This variable will be available to all child processes created by current Bash process ever after

