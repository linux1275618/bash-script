#!/bin/bash

## Topic : Executing commands in script ##

# Installing Dependencies and required packages
echo "########################################"
echo "Installing packages"
echo "########################################"
sudo yum install wget unzip httpd -y 1> /dev/null
echo

# Start and Enable httpd Service
echo "########################################"
echo "Start & Enable Httpd Service"
echo "########################################"
sudo systemctl start httpd
sudo systemctl enable httpd
echo

# Creating Temp Directory
echo "########################################"
echo "Starting Artifact Deployment"
echo "########################################"
mkdir -p /tmp/webfiles
cd /tmp/webfiles
echo 

wget https://www.tooplate.com/zip-templates/2108_dashboard.zip 1> /dev/null
unzip 2108_dashboard.zip 1> /dev/null
mv 2108_dashboard monitoring
sudo cp -r monitoring /var/www/html/
echo

# Bounce Service
echo "########################################"
echo "Restarting httpd Service"
echo "########################################"
systemctl restart httpd
echo 

# Clean Up
echo "########################################"
echo "Clean Up"
echo "########################################"
rm -rf 2108_dashboard.zip monitoring
echo

# httpd Status
echo "########################################"
echo "httpd Status"
echo "########################################"
systemctl status httpd | grep active
echo

# URL
echo "########################################"
echo "URL : "
echo "########################################"
echo "http://192.168.10.10/monitoring"

