#!/bin/bash

### Uptime ###
echo "#########################################"
echo "Uptime : "
uptime
echo 

echo "#########################################"
echo "Memory Status : "
free -hm
echo

echo "#########################################"
echo "Disk Utilization : "
df -Th
