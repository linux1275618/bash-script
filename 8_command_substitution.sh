#!/bin/bash

## Topic : Command Substitution ##

# We need to store uptime of system in a variable

system_uptime=uptime
echo $system_uptime
# Output : update
echo

# We want that uptime command should be executed and its output should be store in variable name.
# For this purpose we have to use command substitution syntax. 
# 1. $()
# 2. ``

system_uptime=$(uptime)
echo $system_uptime
# Output : 08:33:12 up 3:38, 6 users, load average: 0.08, 0.03, 0.05 
echo

system_uptime=`uptime`
echo $system_uptime
# Output : 08:33:12 up 3:38, 6 users, load average: 0.08, 0.03, 0.05
echo

free_memory=`free -m | grep Mem | awk '{print $4}'`
echo "Free RAM is ${free_memory} MB"
