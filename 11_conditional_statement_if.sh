#!/bin/bash

## Topic : Conditional Statement ##

read -p "Enter a number : " NUM
echo

if [ $NUM -gt 100 ]
then
  echo "Number is greater than 100"
  sleep 3
  echo
elif [ $NUM -lt 50 ]
then
  echo "Number is less than 50"
else
  echo "Number is between 50 and 100"
fi
