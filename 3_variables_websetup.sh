#!/bin/bash

### Topic : Variables ###

# Variable Declaration
PACKAGES_TO_INSTALL="httpd unzip wget"
SOFTWARE_URL="https://www.tooplate.com/zip-templates/2108_dashboard.zip"
FILE_NAME="2108_dashboard"
CONTEXT_NAME="monitoring"
TEMPDIR="/tmp/webfiles/"

# Installing Dependencies and required packages
echo "########################################"
echo "Installing packages"
echo "########################################"
sudo yum install $PACKAGES_TO_INSTALL -y 1> /dev/null
echo

# Start and Enable httpd Service
echo "########################################"
echo "Start & Enable Httpd Service"
echo "########################################"
sudo systemctl start httpd
sudo systemctl enable httpd
echo

# Creating Temp Directory
echo "########################################"
echo "Starting Artifact Deployment"
echo "########################################"
mkdir -p $TEMPDIR
## `-p` : If directory is already exists then it wont give any error
cd $TEMPDIR
echo 

wget $SOFTWARE_URL 1> /dev/null
## `-O filename` : To save downloaded file with custom name 
unzip "${FILE_NAME}.zip" -d ./ 1> /dev/null
## `-d path` : To extract content into custom location.(If custom location directory is not present then it will be created)
mkdir -p /var/www/html/$CONTEXT_NAME
sudo cp -r $FILE_NAME/* /var/www/html/$CONTEXT_NAME
echo

# Bounce Service
echo "########################################"
echo "Restarting httpd Service"
echo "########################################"
systemctl restart httpd
echo 

# Clean Up
echo "########################################"
echo "Clean Up"
echo "########################################"
rm -rf $TEMPDIR
echo

# httpd Status
echo "########################################"
echo "httpd Status"
echo "########################################"
systemctl status httpd | grep active
echo

# URL
echo "########################################"
echo "URL : "
echo "########################################"
echo "http://192.168.10.10/${CONTEXT_NAME}"
