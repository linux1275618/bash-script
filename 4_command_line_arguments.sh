#!/bin/bash

## Topic : Command line argument variable ##

echo "Value of 0 is : "
echo $0
## $0 represents name of script
echo

echo "Value of 1 is :"
echo $1
## $1 represents 1st command line argument
echo

echo "Value of 2 is :"
echo $2
## $2 represents 2nd command line argument
echo

echo "Value of 3 is :"
echo $3
## $3 represents 3rd command line argument
echo

echo "Value of 9 is :"
echo $9
## $9 represents 9th command line argument. And this is the last command line argument variable we can have.
echo

echo "Value of 10 is :"
echo "$10"
## $10 will print 10 as output, this is not command line argument variable
echo
