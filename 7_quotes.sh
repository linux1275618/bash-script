#!/bin/bash

## Topic : Quotes ##

name="Shivam Palaskar"

echo "My Name is" $name
# Output : My Name is Shivam Palaskar

echo 'My Name is' $name
# Output : My Name is Shivam Palaskar
echo

echo "My Name is $name"
# Output : My Name is Shivam Palaskar
## $ have special meaning in double quote. It is a special character in double quote,

echo 'My Name is $name'
# Output : My Name is $name
echo

echo "My Name is ${name}"
# Output : My Name is Shivam Palaskar

echo 'My Name is ${name}'
# Output : My Name is ${name}
echo

echo "My Name is $name, and my salary is $2000 per month"
# output : My Name is Shivam Palaskar, and my salary is 000 per month 
echo

echo 'My Name is $name, and my salary is $2000 per month'
# output : My Name is $name, and my salary is $2000 per month 
echo

echo "My Name is $name, and my salary is '$2000' per month"
# output : My Name is Shivam Palaskar, and my salary is '000' per month 
echo

echo "My Name is $name, and my salary is \$2000 per month"
# output : My Name is Shivam Palaskar, and my salary is $2000 per month
## $ have special meaning in double quote. It is a special character in double quote,
## If we have a need where $ should not be treated as special charater in double quotes then we must us escape chatacter before $. i.e. \$
echo
