#!/bin/bash

## Topic : User Input ##

echo "Enter Your Name : "
read NAME

read -p "Age : " AGE
# `-p` : stand for promt, print the given statement and also wait for user input
read -sp "Password : " PASSWORD
# `-s` : stand for suppress. For password like input not visible on screen while typing
echo

echo "Welcome ${NAME}"
