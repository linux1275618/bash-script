#!/bin/bash

### Topic : Variables ###

# Variable Declaration
# $1 : PACKAGES_TO_INSTALL="httpd unzip wget"
# $2 : TEMPDIR="/tmp/webfiles/"
# $3 : SOFTWARE_URL="https://www.tooplate.com/zip-templates/2108_dashboard.zip"
# $4 : FILE_NAME : "2108_dashboard"
# $5 : CONTEXT_NAME="monitoring"

# Installing Dependencies and required packages
echo "########################################"
echo "Installing packages"
echo "########################################"
sudo yum install $1 -y 1> /dev/null
echo

# Start and Enable httpd Service
echo "########################################"
echo "Start & Enable Httpd Service"
echo "########################################"
sudo systemctl start httpd
sudo systemctl enable httpd
echo

# Creating Temp Directory
echo "########################################"
echo "Starting Artifact Deployment"
echo "########################################"
mkdir -p $2
## `-p` : If directory is already exists then it wont give any error
cd $2
echo 

wget $3 1> /dev/null
## `-O filename` : To save downloaded file with custom name 
unzip "${4}.zip" -d ./ 1> /dev/null
## `-d <path>` : To extract content into custom location.(If custom location directory is not present then it will be created)
sudo mkdir -p /var/www/html/$5
sudo cp -r $4/* /var/www/html/$5
echo

# Bounce Service
echo "########################################"
echo "Restarting httpd Service"
echo "########################################"
systemctl restart httpd
echo 

# Clean Up
echo "########################################"
echo "Clean Up"
echo "########################################"
rm -rf $2
echo

# httpd Status
echo "########################################"
echo "httpd Status"
echo "########################################"
systemctl status httpd | grep active
echo

# URL
echo "########################################"
echo "URL : "
echo "########################################"
echo "http://192.168.10.10/${5}"
